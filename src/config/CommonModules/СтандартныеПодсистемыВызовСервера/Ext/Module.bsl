﻿// Дополнения к методам БСП
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Устанавливает флаг обновления информационной базы в случае 
// 	при наличии компонент, требующих обновления
// 	
&Вместо("ОбработатьПараметрыКлиентаНаСервере")
Процедура пнобсп_ОбработатьПараметрыКлиентаНаСервере(Знач Параметры)
	
	Если пнобсп_Модуль.пно_ИнформационнаяБаза().ОбновлениеЕсть() Тогда
		Параметры.ПараметрЗапуска = Параметры.ПараметрЗапуска + " ЗапуститьОбновлениеИнформационнойБазы";
	КонецЕсли;
	
	ПродолжитьВызов(Параметры);
	
КонецПроцедуры

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
